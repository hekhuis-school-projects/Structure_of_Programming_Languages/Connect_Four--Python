#!/usr/bin/env python

"""
(This is a Python implementation of ConnectFour.
Was originally created in C.)

Connect Four is a program created for CIS 343:
Structure of Programming Languages at Grand Valley
State University during the Winter 2017 semester.
In this program you play a game of Connect Four
(https://en.wikipedia.org/wiki/Connect_Four)
except the user can define the width and height
of the board as well as the amount needed to
connect in a row to win. Designed for Linux.

@author Kyle Hekhuis
"""
import argparse
import sys
import pickle
import os.path

"""
Class holding the game state which is the
width, height, connect amount, current player,
2D board, game over status, and player scores.
"""
class GameState(object):
	def __init__(self, width, height, connect, currentPlayer, score):
		self.width = width
		self.height = height
		self.connect = connect
		self.currentPlayer = currentPlayer
		#Prefer to do this way, but need a lambda for project requirments
		#self.board = [['*' for x in range(width)] for y in range(height)]
		self.board = map(lambda x: map(lambda y: '*', range(width)), range(height))
		self.gameOver = False
		self.score = score

#Terminal print out color codes
RED	= "\x1B[31m"
CYAN = "\x1B[36m"
MAGENTA	= "\x1B[35m"
RESET = "\x1B[0m"
BOLD = "\x1B[1m"

#Constant strings
P1 = CYAN + "Player 1" + RESET
P2 = MAGENTA + "Player 2" + RESET;
INGAME_HELP = ("Place a piece by entering the column number.\n" 
			  + CYAN + "Player 1" + RESET + " is " 
			  + CYAN + "'X'" + RESET + " and " 
			  + MAGENTA + "Player 2" + RESET + " is " 
			  + MAGENTA + "'O'" + RESET + ".\nYou "
			  + "can save or load at anytime by entering "
			  + "'-s filename' to save and '-l filename' to load. "
			  + "To quit, type '-q'. For help, type '-h'.")

"""
Gets command line arguments and starts the connect four game.
"""
def main():
	args = setupParser()
	if args.load is not None:
		tempGS = load(args.load)
		if tempGS is not None:
			gs = tempGS
			playGame(gs, True)
		else:
			exit()
	else:
		gs = GameState(args.width, args.height, args.connect, 1, [0,0])
		playGame(gs, False)

"""
Runs the game with while loop and gets user input.

@param (GameState)gs object containing the game state
@param (Boolean)loaded boolean for if command line arg to
										load game was passed.
"""
def playGame(gs, loaded):
	#type check
	if type(gs) is not GameState or type(loaded) is not bool:
		print "Incorrect params passed to playGame."
		exit()

	clearScreen()
	if (loaded):
		print BOLD + "Game loaded successfully!\n" + RESET
	while True:
		print(BOLD +  CYAN + "CONNECT" + MAGENTA 
				+ " 'FOUR'!\n" + RESET + "===============")
		print('{}'.format(INGAME_HELP))
		print("\nScore is:\n{}: {}\n{}: {}".format(P1, gs.score[0], P2, gs.score[1]))

		while (not gs.gameOver):
			printBoard(gs)
			print("\nIt is {}'s turn".format(P1 if gs.currentPlayer == 1 else P2))
			#Get user input
			input = sys.stdin.readline()
			#Remove the '\n' from input
			input = input.strip('\n')
			#Try to convert to int. If it's not int, set col to 0 so it doesn't
			#hit the place piece if statement. Error will occur if it does.
			try:
				col = int(input)
			except ValueError:
				col = 0
			#Check for load command
			if (input.startswith("-l ")):
				tempGS = load(input[3:])
				if tempGS is not None:
					gs = tempGS
					clearScreen()
					print BOLD + "Game loaded successfully!\n" + RESET
					print('{}'.format(INGAME_HELP))
					print("\nScore is:\n{}: {}\n{}: {}".format(P1, gs.score[0], P2, gs.score[1]))
			#Check for save command
			elif (input.startswith("-s ")):
				save(gs, input[3:])
			#Check for quit command
			elif (input == "-q"):
				return
			#Check for help command
			elif (input == "-h"):
				print("\n{}".format(INGAME_HELP))
			#Get col to place piece and check if valid
			elif (col > 0 and col <= gs.width):
				if (placePiece(col, gs)):
					nextPlayer(gs)
			#Invalid input if nothing else hit
			else:
				print RED + "\aInvalid input! Type -h for help.\n" + RESET

		print("\nWould you like to play another game?\n"
			     "Enter '-y' for yes, anything else for no.")
		#Get user input
		input = sys.stdin.readline()
		#Remove the '\n' from input
		input = input.strip('\n')
		if (input == "-y"):
			gs.gameOver = False
			gs.board = map(lambda x: map(lambda y: '*', range(gs.width)), range(gs.height))
			#Reset to first player
			gs.currentPlayer = 1;
			#Clear screen so user doesn't get confused seeing
			#previous game print outs
			clearScreen()
		else:
			return

"""
Prints the state of the game board.
'X' represent player 1's pieces and
'O' represent player 2's pieces.
Uses sys.stdout.write instead of print for formatting
since print always does a new line whereas sys.stdout.write
does not

@param (GameState)gs object containing the game state
"""		
def printBoard(gs):
	#type check
	if type(gs) is not GameState:
		print "Incorrect params passed to printBoard."
		exit()

	print("\n")
	for i in range(gs.height):
		for j in range(gs.width):
			if (gs.board[i][j] == 'X'):
				#Print 'X's in cyan
				sys.stdout.write(CYAN + "{}".format(gs.board[i][j]) + RESET)
			elif (gs.board[i][j] == 'O'):
				#Print 'O's in magenta
				sys.stdout.write(MAGENTA + "{}".format(gs.board[i][j]) + RESET)
			else:
				sys.stdout.write(gs.board[i][j])

		sys.stdout.write("\n")

"""
Method created by Professor Ira Woodring.
Clears the 'screen' AKA the terminal the
game is being played in by printing a lot
of new line characters.
"""
def clearScreen():
	for i in range(50):
		print "\n"

"""
Advances the current player to
the next player.

@param (GameState)gs object containing the game state
"""
def nextPlayer(gs):
	#type check
	if type(gs) is not GameState:
		print "Incorrect params passed to nextPlayer."
		exit()

	if (gs.currentPlayer == 1):
		gs.currentPlayer = 2
	else:
		gs.currentPlayer = 1;

"""
Places a piece in the specified column position on
the board for the current player.

@param (int)col column to place piece
@param (GameState)gs object containing the game state
@return True if piece placed successfully,
 		     False if not
"""
def placePiece(col, gs):
	#type check
	if type(col) is not int or type(gs) is not GameState:
		print "Incorrect params passed to placePiece."
		exit()

	#To line up with array since array starts at 0.
	col -= 1
	for i in range(gs.height - 1, -1, -1):
		if (gs.board[i][col] == '*'):
			if (gs.currentPlayer == 1):
				gs.board[i][col] = 'X'
			else:
				gs.board[i][col] = 'O'
			checkWin(gs, col, i)
			return True
	print RED + "\aCan't place piece there! Try again!\n" + RESET
	return False

"""
Checks to see if winning conditions have been met
in the game by calling the functions to check
vertical, horizontal, and diagonal wins. Also
checks for draw by seeing if top row on board is
full with no wins.

@param (GameState)gs object containing the game state
@param (int)col column the piece was placed in
@param (int)row row the piece was placed in
"""
def checkWin(gs, col, row):
	#type check
	if type(gs) is not GameState or type(col) is not int or type(row) is not int:
		print "Incorrect params passed to checkWin."
		exit()

	player = 'X'
	if (gs.currentPlayer == 2):
		player = 'O'

	if (checkVertical(gs, col, row, player) or
		checkHorizontal(gs, col, row, player) or
		checkDiagonal(gs, col, row, player)):

		printBoard(gs)
		print BOLD + "\nPlayer {} has won!\n".format(gs.currentPlayer) + RESET
		gs.gameOver = True;
		gs.score[gs.currentPlayer - 1] += 1

	#Check for draw by checking that top row is full
	#and game hasn't been won
	if (not gs.gameOver):
		amountLeft = 0
		for i in range(gs.width):
			if (gs.board[0][i] == '*'):
				amountLeft += 1
		if (amountLeft == 0):
			gs.gameOver = True
			printBoard(gs)
			print BOLD + "\nDraw! All spots occupied and no winner!\n" + RESET

"""
Checks to see if winning conditions have been met
in the vertical direction of placed piece. Does
so by checking if there are similar pieces below
the placed piece and incrementing a counter for
each time there is. If the counter is equal to
the amount needed to connect in a row for a win,
it returns true.

@param (GameState)gs object containing the game state
@param (int)col column the piece was placed in
@param (int)row row the piece was placed in
@param (str)player char representation of player who
							  placed piece
@return True if win has been found vertically
"""
def checkVertical(gs, col, row, player):
	#type check
	if type(gs) is not GameState or type(col) is not int or type(row) is not int or type(player) is not str:
		print "Incorrect params passed to checkVertical."
		exit()

	#Impossible for vertical win to happen if connect 
	#is greater than the height dimension.
	if (gs.connect > gs.height):
		return False

	#Set count to 1 since starting point counts as 1
	count = 1

	#Checks below the placed piece on the board.
	#Don't have to check above since impossible to
	#have that in this game.
	#for (int i = row + 1; i < gs->height; i++)
	i = row + 1
	while (i < gs.height):
		if (gs.board[i][col] == player):
			count += 1
		else:
			break
		i += 1

	#If count is equal to amount needed to connect in
	#a row return true for a win.
	if (count == gs.connect):
		return True
	else:
		return False		

"""
Checks to see if winning conditions have been met
in the horizontal direction of placed piece. Does
so by checking if there are similar pieces on
either side of the placed piece and incrementing a
counter for each time there is. If the counter is 
equal to the amount needed to connect in a row for
a win, it returns true.

@param (GameState)gs object containing the game state
@param (int)col column the piece was placed in
@param (int)row row the piece was placed in
@param (str)player char representation of player who
							   placed piece
@return True if win has been found horizontally
"""
def checkHorizontal(gs, col, row, player):
	#type check
	if type(gs) is not GameState or type(col) is not int or type(row) is not int or type(player) is not str:
		print "Incorrect params passed to checkHorizontal."
		exit()

	#Impossible for horizontal win to happen if connect 
	#is greater than the width dimension.
	if (gs.connect > gs.width):
		return False

	#Set count to 1 since starting point counts as 1
	count = 1

	#Check for matching pieces to the left of starting point
	i = col - 1
	while (i >= 0):
		if (gs.board[row][i] == player):
			count += 1
		else:
			break
		i -= 1

	#Check for matching pieces to the right of starting point
	i = col + 1
	while (i < gs.width):
		if (gs.board[row][i] == player):
			count += 1
		else:
			break
		i += 1

	#If count is equal to amount needed to connect in
	#a row return true for a win.
	if (count == gs.connect):
		return True
	else:
		return False

"""
Checks to see if winning conditions have been met
in the diagonal direction of placed piece. Does
so by checking if there are similar pieces kitty
corner to the placed piece and incrementing a 
counter for each time there is. If the counter is
equal to the amount needed to connect in a row for
a win, it returns true.

@param (GameState)gs object containing the game state
@param (int)col column the piece was placed in
@param (int)row row the piece was placed in
@param (str)player char representation of player who
							   placed piece
@return true if win has been found diagonally
"""
def checkDiagonal(gs, col, row, player):
	#type check
	if type(gs) is not GameState or type(col) is not int or type(row) is not int or type(player) is not str:
		print "Incorrect params passed to checkDiagonal."
		exit()

	#Impossible for diagonal win to happen if connect
	#is greater than the min dimension.
	if (gs.connect > gs.width or gs.connect > gs.height):
		return False

	#Check the / diagonal
	#========================
	#Set count to 1 since starting point counts as 1
	count = 1
	#Check southwest of point
	i = row + 1
	j = col - 1
	while (i < gs.height and j >= 0):
		if (gs.board[i][j] == player):
			count += 1
		else:
			break
		i += 1
		j -= 1

	#Check northeast of point
	i = row - 1
	j = col + 1
	while (i >= 0 and j < gs.width):
		if (gs.board[i][j] == player):
			count += 1
		else:
			break
		i -=1
		j += 1

	#If count is equal to amount needed to connect in
	#a row return true for a win.
	if (count == gs.connect):
		return True

	#Check the \ diagonal
	#========================
	#Set count to 1 since starting point counts as 1
	count = 1
	#Check southeast of point
	i = row + 1
	j = col + 1
	while (i < gs.height and j < gs.width):
		if (gs.board[i][j] == player):
			count += 1
		else:
			break
		i += 1
		j += 1

	#Check northwest of point
	i = row - 1
	j = col - 1
	while (i >= 0 and j >= 0):
		if (gs.board[i][j] == player):
			count += 1
		else:
			break
		i -= 1
		j -= 1

	#If count is equal to amount needed to connect in
	#a row return true for a win.
	if (count == gs.connect):
		return True
	return False

"""
Saves the current game state into the specified file.

@param (GameState)gs game state to save
@param (str)filename name of file to save to
"""
def save(gs, filename):
	#type check
	if type(gs) is GameState:
		saveFile = open(filename, 'wb')
		pickle.dump(gs, saveFile)
		saveFile.close()
		print BOLD + "\nGame saved successfully as file with name '{}'!".format(filename) + RESET
	else:
		print RED + "\n\aFile failed to save!" + RESET

"""
Loads the game state saved in the specified file.

@param (str)filename name of file to load from
"""
def load(filename):
	try:
		loadFile = open(filename, 'r')
		gs = pickle.load(loadFile)
		return gs
	except:
		print RED + "\aFile failed to load!" + RESET
		return None

"""
Sets up variables for argument parser with passed command line arguments.
"""
def setupParser():
	parser = argparse.ArgumentParser(prog="ConnectFour.py",
									 description="Connect Four is a game where two players drop "
									 "colored tiles (represented here by 'X' for "
									 "player 1 and 'O' for player 2) into a game "
									 "board in such a way that four (or user specified"
									 " amount) similarly colored tiles line up "
									 "either horizontally, vertically, or diagonally."
									 " The first person to \"connect four\" tiles in "
									 "such a manner wins. The size of the board and "
									 "amount needed to connect to win can be specified "
									 "by the user. Use --help to see commands.",
									add_help=False)
	parser.add_argument("-?", "--help", action="help", 
						help="Give this help list.")
	parser.add_argument("-w", "--width", type=int, default=7,
						help="Set width of board to WIDTH.")
	parser.add_argument("-h", "--height", type=int, default=7,
						help="Set height of board to HEIGHT.")
	parser.add_argument("-s", "--square", type=int, default=7,
						help="Set board to square with size SQUARE.")
	parser.add_argument("-c", "--connect", type=int, default=4,
						help="Set CONNECT as number of pieces in a row needed to win.")
	parser.add_argument("-l", "--load", default=None,
						help="Load saved game state from LOAD.")

	args = parser.parse_args()

	if (args.square != 7):
		args.width = args.square
		args.height = args.square
		
	errorCheck(args.width, args.height, args.square, args.connect)
	
	return args

"""
Checks to see if passed parameters are valid or not. Gives
error and exits program if one of the parameters is wrong.

@param (int)width width param to check
@param (int)height height param to check
@param (int)square square param to check
@param (int)connect connect param to check
"""
def errorCheck(width, height, square, connect):
	if type(width) is not int or type(height) is not int or type(square) is not int or type(connect) is not int:
		print "Incorrect params passed to errorCheck."
		exit()
	
	if (width < 1 or height < 1 or square < 1 or connect < 1):
		print("Entered in a wrong parameter value.\n"
			  "All parameters must be greater than 0.\n")
		exit()
		
	#Get largest dimension of board. Connect can't be
	#greater than that.
	max = width if width > height else height
	if (connect > max):
		print("Amount needed to connect for a win can't be "
			  "greater than the largest dimension of the "
			  "board.");
		exit()

if __name__ == "__main__":
	main()
